﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DAMAGE : MonoBehaviour
{
   void OnTriggerEnter2D(Collider2D o)
    {
        if(o.tag == "Player")
        {
            Destroy(o.gameObject);
        }
    }
}
