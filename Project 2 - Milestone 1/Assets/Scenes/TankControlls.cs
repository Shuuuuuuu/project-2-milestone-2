﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankControlls : MonoBehaviour
{
    public float moveSpeed = 1;
    public float rotateSpeed = 12;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float moveVector = Input.GetAxis("Vertical");
        float rotateVector = Input.GetAxis("Horizontal");


        this.transform.Translate(0f, moveVector * moveSpeed * Time.deltaTime, 0f);
        this.transform.Rotate(0f, 0f, -rotateVector * (rotateSpeed * 10) * Time.deltaTime);
    }

}
